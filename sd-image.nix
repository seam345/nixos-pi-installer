# This module defines a small NixOS installation CD.  It does not
# contain any graphical stuff.
{config, pkgs, ...}:
let
  yubikeyPubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkrrhqdyMysvZ1VTsRpuVbpZ4FpEGBzJLdDhj5GMNbCJvNC8jgLnERI1Ubb1E18B9e9+ArzmXlEvSHMI6g/vdADa2UPQnDKEtm+ZaLhLCqZaiHPshbAs/q70lqggaswHYc/RW5uT9ELqSkSO2kOfotDK7gx9TYukmqIoZXY60r/RQBvWDz0G6mswIQ6hdJv4H9boPbKT49E14T7ycoJPkGKRGKCnDTmA19AgzF41+iLQFY+PfBpICpBix2NEkKRB2ZR0rhHkq9EgCJq5ZHAxdMJ57PXcxn12m0JB6gjx6MskQ3rlQbr245f/+OwGgbdBmOdlbsB6stQlOCKL1kR/wOMVni+236SmHGACEQXGfVhYdlE3XU+18Ju+vY1Su09tOtLJP1/Ys/2N2oh2C+cctK36K56wWjB0H+IFIDaaBoL0xvWRPBI8S+xtWwYGsUCP5vJBVNBteGww0SksxrgA/q5nKXLVi5BYBFWhQndhP4WnkkVSOSXqNCZKH89+yJmRs= cardno:000614465514";
  laptopPubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCswlyW1qQ6QCdkchTualqdLgsjaR177Ud9f0+z++dkOnfRQLljcCljh1fdR1pGahBlqzonF4zMXAF873FG/d/H/TwRQQyWtnb4AriddOdV3jJnoKafvHDUxBb40LtvRM6c8d4klyMgw2+EK223OTsQifh8NIuZkqJqyhFuLX5Gtbz96EmXWmbiQsUBL5f131etKZCfFSyX3HbGTeUSCg4on//QXUQER60CW9jr7xeSrnwTuWls2l19IkWQS08/7/GKqRKriLkpgcHIDBpRYc7MUyaZCzEB/3DJ/l/fG8BbtzMCXaHQPMaAOGRI9eQIeWTiyAQf7Pb3RoGudp780845xCLvlcusv+ehR/v6btMssDSBJVkc2axLZdQN8n2muz+tcvVxWA/BUiWpB16/IHY3bBx7v9zvDilEq1orCQDIrq6WlpgssX5oX61dCIDfRi+3amZ1km2fm89NBV39so24wmr8LUlwf/397w+yAnpHr23qCWgT3fMNIvtAhdDjV2M= sean@lenovo-laptop";
in
{
  imports = [
    <nixpkgs/nixos/modules/installer/sd-card/sd-image-aarch64.nix>
  ];

    security.sudo.enable = true;
  security.sudo.extraConfig = ''
    krops ALL=(ALL) /run/current-system/sw/bin/nixos-rebuild -I /var/src switch
    krops ALL=(ALL) /run/current-system/sw/bin/nixos-rebuild -I /var/src switch --no-build-nix
    '';
#  security.sudo.extraRules."nixos-rebuild switch".users = [ "krops" ];

    users.users.krops = { # remember krops needs to have access to /var/src
       isNormalUser = true;
       home = "/home/krops";
       description = "krops";
#       extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
       openssh.authorizedKeys.keys = [ "${laptopPubSSHKey}"];

     };


  # Enable SSH in the boot process.
  services.openssh.enable = true;
  systemd.services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
  users.users.root.openssh.authorizedKeys.keys = [
    "${yubikeyPubSSHKey}"
  ];


}